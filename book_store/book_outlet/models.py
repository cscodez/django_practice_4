from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.text import slugify
from django.urls import reverse

# Create your models here.


class Country(models.Model):
    name = models.CharField(max_length=80)
    code = models.CharField(max_length=2)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Countries"


class Address(models.Model):
    street = models.CharField(max_length=80)
    postal_code = models.CharField(max_length=5)
    city = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.street}, {self.postal_code}, {self.city}"

    class Meta:
        verbose_name_plural = "Address Entries"
    # Django looking for this subclass, if set up, it changes the plural display name on admin page


class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.OneToOneField(Address, on_delete=models.CASCADE, null=True)
    # One-to-one relation (an author has one address)

    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    def __str__(self):
        return self.full_name()


class Book(models.Model):
    # id is being given automatically by Django to each inserts
    title = models.CharField(max_length=50)
    rating = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True, related_name="books")
    # One-to-many relation: ForeignKey (author can have many books)
    # CASCADE: if author deleted, related books deleted
    # more options like CASCADE: PROTECT - not to delete in relation, SET_NULL author will be set to null
    is_bestselling = models.BooleanField(default=False)
    slug = models.SlugField(default="", blank=True, null=False, db_index=True)
    # db_index to improve performance
    published_countries = models.ManyToManyField(Country)
    # There is an add method for the many-to-many connection, which you can add a country to published_countriesm

    def get_absolute_url(self):
        return reverse("book-detail", args=[self.slug])

    # when a Book is getting printed to console, how it should look like:
    def __str__(self):
        return f"{self.title} ({self.rating})"

